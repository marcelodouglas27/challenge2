public class Person  {
    
    
    public Person(String name, String nationality, String biggestDream){
        this.name = name;
        this.nationality = nationality;
        this.biggestDream = biggestDream;
    }
    
    
    private String name;
    
    public String getName() {
        return name;
    }
    
    private String nationality;
    
    public String getNationality() {
        return nationality;
    }
    
    private String biggestDream;
    
    public String getBiggestDream() {
        return biggestDream;
    }
    
}